#!/usr/bin/python3

# a command to convert xls to xlsx on linux with Libreoffice installed is 
# $ soffice --convert-to xlsx test.xls
# batch operation is supported via star (*.xls)

# width 100 almost exactly corresponds to 18X5=90 chars of 12 size noto sans mono
# which means height of 12.5 per row and width 100/90~=1.11 per char

#from openpyxl.utils import column_index_from_string
from bs4 import BeautifulSoup
from time import sleep
from openpyxl.utils import get_column_letter
from openpyxl.styles import Alignment, Font, PatternFill
from openpyxl import load_workbook
from openpyxl import Workbook
from docxtpl import DocxTemplate
from docxtpl import Listing
from datetime import datetime, date
import simplejson as json
import sys
import requests

target_file = ''
output_file = ''
template_file = ''
docx_out = ''
suspects = []
companies = [] #maybe they need to be the same as suspects?
exceptional_trades = []
complicated_enabled = True
verbose = False
cmd = sys.argv[0]
rusprofile_requests=False
ogrn_requests = False
ogrn_key = ''

# ------------------ Navigation constants---------------------------------

IST_BUDGET_NAV={
        'DATE' :                'A',
        'NUMBER' :              'B',
        'TITLE' :               'D',
        'COMPETITOR_TITLE' :    'J',
        'COMPETITOR_INN' :      'K',
        'COMPETITOR_PRICE' :    'Q',
        'INITIAL_PRICE' :       'P',
        'PRICE_DROP' :          'R',
        'COMPETITOR_NUMBER' :   'S',
        'TYPE' :                'C'
        }

SPARK_NAV={
        'DATE' :                'K',
        'NUMBER' :              'E',
        'TITLE' :               'H',
        'COMPETITOR_TITLE' :    'O',
        'COMPETITOR_INN' :      'P',
        'COMPETITOR_PRICE' :    'S',
        'INITIAL_PRICE' :       'D',
        'PRICE_DROP' :          'T',
        'TYPE' :                'V',
        'WINNER' :              'Q',
        'ALLOWED' :             'R',
        'LEVEL' :               'A',
        'FORM':                 'U',
        'PROVIDER_INFO' :       'B17'
        
        }


OUT_DATE_COL =              'C'
OUT_NUMBER_COL =            'A'
OUT_TITLE_COL =             'B'
OUT_COMPETITOR_TITLE_COL =  'G'
OUT_COMPETITOR_PRICE_COL =  'H'
OUT_INITIAL_PRICE_COL =     'D'
OUT_WINNER_PRICE_COL =      'F'
OUT_WINNER_TITLE_COL =      'E'
OUT_PRICE_DROP_COL =        'I'
OUT_COMMENT_COL =           'J'

COMPLIST_TITLE_COL =    'A'
COMPLIST_INN_COL =      'B'
COMPLIST_DIRECTOR_COL = 'C'
COMPLIST_FOUNDER_COL =  'D'
COMPLIST_SHARE_COL =    'E'
COMPLIST_PHONE_COL =    'G'
COMPLIST_SOURCE_COL =   'F'

COMPANY_WS_TITLE =      'лист 1 (компании)'
AUCTIONS_WS_TITLE =     'лист 2 (торги)'


# ------------------ Visual constants---------------------------------



COMPANY_TITLE_COL_WIDTH_MAX = 50
TITLE_COL_WIDTH_MAX = 100
COMPANY_TITLE_COLS = [OUT_COMPETITOR_TITLE_COL, OUT_WINNER_TITLE_COL]
PRICE_COLS = (OUT_INITIAL_PRICE_COL, OUT_COMPETITOR_PRICE_COL, OUT_WINNER_PRICE_COL)
NUMBER_COLS = PRICE_COLS + (OUT_NUMBER_COL,OUT_DATE_COL,OUT_PRICE_DROP_COL)
HEIGHT_PER_ROW = 12.5
INTERROW_HEIGHT = HEIGHT_PER_ROW*(12/23)
#WIDTH_PER_CHAR = 105.0/90.0  for liberation mono or noto sans mono
#WIDTH_PER_CHAR = (107.0/90.0)*(19.0/18.0)*(149.0/142.0) # empyrical for consolas. Need to simplify this.
WIDTH_PER_CHAR = 1.32
HMARGIN = 2.5

font = Font(name='Consolas',
    size=12,
    bold=False,
    italic=False,
    vertAlign=None,
    underline='none',
    strike=False,
    color='FF000000')

title_font = Font(name='Consolas',
    size=12,
    bold=True,
    italic=False,
    vertAlign=None,
    underline='none',
    strike=False,
    color='FF000000')

alignment=Alignment(horizontal='center',
    vertical='center',
    text_rotation=0,
    wrap_text=True,
    shrink_to_fit=False,
    indent=0)

normal_fill = PatternFill(fill_type=None,
     start_color='FFFFFFFF',
     end_color='FF000000')

warning_fill = PatternFill(fill_type='solid',
     start_color='FFFFFF00',
     end_color='FFFFFF00')

error_fill = PatternFill(fill_type='solid',
     start_color='FFFF0000',
     end_color='FF000000')

WINNER_UNKNOWN = 'winner unknown or malformed'
DOUBLE_WINNER = 'double winner'
DUPLICATE_COMPETITOR = 'duplicate competitor'

# ------------------ Network constants---------------------------------

RUSPROFILE_SHORT='rusprofile.ru'
RUSPROFILE_COMMON = "https://www.rusprofile.ru/id/" #we don't need to look up individuals
YAHOOCOJP_COMMON = "https://search.yahoo.co.jp/search;?p="
OGRN_ROOT = 'https://ogrn.site/'
OGRN_COMMON = OGRN_ROOT+'интеграция/'
USER_AGENT = 'Anticorruption Cartel Checker Bot'
COOLDOWN = 3

# ------------------ Classes---------------------------------

class SourceTable(object):
    #def __init__(self,nav={},start=2):
        #self.navigation=nav
        #self.starting_index=start
    def __init__(self,wb):
        self.wb=wb
    starting_index=2
    
    def __new__(cls,wb):
        
        #print ('internal value is: '+ws['A1'].value)
        if cls is SourceTable:
            if any([('поиск' in x) for x in wb.sheetnames]):
                print ('source recognized as Spark')
                return super(SourceTable,cls).__new__(Spark)
            if 'дата' in wb.active['A1'].value.lower():
                print ('source recognized as Ist-Budget')
                return super(SourceTable,cls).__new__(IstBudget)
            print('Error: Source not recognized!')
            sys.exit(1)
        else:
            return super(SourceTable, cls).__new__(cls, wb)
    
    def to_trades(self):
        trades = []
        return trades

class IstBudget(SourceTable):
    navigation = IST_BUDGET_NAV
    
    def to_trades (self):
        ws=self.wb.active
        trades = []
        limit = len(ws[self.navigation['NUMBER']+':'+self.navigation['NUMBER']])
        i=self.starting_index
        while i != limit:
            number = ''
            candidate = ws[self.navigation['NUMBER']+str(i)].value
            if isinstance(candidate, str):
                number = candidate.lstrip(' ').rstrip(' ').rstrip('\n')
            if validate_number(number):
                #print (str(i)+': '+number)
                trade = self.process_entry(ws, i, suspects)
                trades.append(trade)
            i += 1
        return trades
    
    def process_entry (self, ws, index, suspect_list):
        
        # print ('intraclass processor called')
        trade = Trade()
        nav = self.navigation
        
        comps=int(ws[nav['COMPETITOR_NUMBER']+str(index)].value)
        competitors = []
        inncol = nav['COMPETITOR_INN']
        
        for c in ws[(inncol+str(index)):(inncol+str(index+comps))]:
            competitor = Competitor()
            row = str(c[0].row)
            competitor.inn = c[0].value
            competitor.title =          ws[nav['COMPETITOR_TITLE']+row].value
            competitor.price =          ws[nav['COMPETITOR_PRICE']+row].value
            competitor.price_drop =     ws[nav['PRICE_DROP']+row].value
            competitors.append(competitor)
        
        trade.start_date=           dt2date(datetime.strptime(ws[nav['DATE']+str(index)].value,"%d.%m.%Y"))
        trade.regnum=               ws[nav['NUMBER']+str(index)].value
        trade.title=                ws[nav['TITLE']+str(index)].value
        trade.init_price=           ws[nav['INITIAL_PRICE']+str(index)].value
        trade.type =                ws[nav['TYPE']+str(index)].value
        trade.winner = competitors[0]
        trade.competitors = competitors
        
        return trade
    
class Spark(SourceTable):
    navigation = SPARK_NAV
    starting_index = 3
    def to_trades (self):
        
        sheetname = self.wb.sheetnames[1]
        print('opening sheet: ' + sheetname)
        ws=self.wb[sheetname]
        trades = []
        limit = len(ws[self.navigation['LEVEL']+':'+self.navigation['LEVEL']])
        print ('initial row count: ' + str(limit))
        i=self.starting_index
        while i != limit:
            level = int(ws[self.navigation['LEVEL']+str(i)].value)
            if level == 1:
                trade = self.process_entry(ws, i, suspects)
                if trade != None:
                    trades.append(trade)
#                    if trade.comment == '':
#                        trades.append(trade)
#                    else:
#                        exceptional_trades.append(trade)
            i += 1
        return trades
    
    def process_entry (self, ws, index, suspect_list):
        
        #print ('intraclass processor called')
        
        nav = self.navigation
        if not 'торг' in  ws[nav['FORM']+str(index)].value.lower(): #we don't need contracts (Spark-specific filter)
            #print ('skipping contract at ' + str(index))
            return None
        
        trade = Trade()
        seconds = 0
        while ws[nav['LEVEL']+str(index+seconds+1)].value == 2:
            seconds+=1
        competitors = []
        inncol = nav['COMPETITOR_INN']
        #print ('our iterable is ' + str(len (ws[(inncol+str(index)):(inncol+str(index+seconds))]))+' long')
        trade.regnum=               str(ws[nav['NUMBER']+str(index)].value)
        #print("processing regnum ", trade.regnum)
        
        for c in ws[(inncol+str(index)):(inncol+str(index+seconds))]:
            
            row = str(c[0].row)
            
            if not validate_inn(str(c[0].value)):
                #print ('skipping spacer')
                continue
            if 'не ' in ws[nav['ALLOWED']+row].value.lower(): #ignore disallowed competitors. Space intended.
                #print ('skipping a disqualified competitor at row ' + row)
                continue # it is important to check this BEFORE checking for price drops(because unqualified don't have one)
            
            pricedrop_value =           ws[nav['PRICE_DROP']+row].value
            if pricedrop_value == None:
                #print ( row + '\t - no pricedrop - skipping such competitor') # this can lead to 0 competitors but it is ok!
                continue
                #return None
            
            competitor = Competitor()
            competitor.inn =            str(c[0].value)
            competitor.title =          self.link2name(ws[nav['COMPETITOR_TITLE']+row].value)
            competitor.price =          ws[nav['COMPETITOR_PRICE']+row].value
            competitor.price_drop =     (pricedrop_value*100*100//1)/100
            iswinner = ws[nav['WINNER']+row].value != None
            #print ('iswinner before comparison ', str(ws[nav['WINNER']+row].value))
            if  iswinner:
                #print("new winner found, inn ", competitor.inn)
                #print ('previous winner is',trade.winner,trade.winner.inn,trade.winner.price)
                if trade.winner.price != 0:
                    #print (trade.regnum + '\t double winner')
                    trade.comment = DOUBLE_WINNER
                    trade.fill = error_fill
                    continue
                trade.winner = competitor
            duplicates = [x for x in competitors if competitor.inn == x.inn]
            if len(duplicates)!=0: #prevent competitor duplication
                #print (trade.regnum + '\t duplicate competitor')
                if trade.comment == '':
                    trade.comment = DUPLICATE_COMPETITOR
                    trade.fill = warning_fill
                    #continue # don't append itself
#                if len(duplicates)>1:
#                    print('Error, duplicate overflow at index ' + str(index) + ' - skipping' )
#                    return None
#                if iswinner:
#                    print ('passing winnership from new duplicate')
#                    trade.winner = duplicates[0] # pass a winnership to an already-existing duplicate
#                print ('Duplicate ' + competitor.title + ' - not writing') 
            
            competitors.append(competitor)
        
        trade.competitors = competitors
        
        datestring =                str(ws[nav['DATE']+str(index)].value).strip().split(' ')[0] #STUPIDITY ALERT!
        trade.start_date=           dt2date(datetime.strptime(datestring,"%Y-%m-%d")) #for some stupid reason plain date doesn't have strptime. WHY&
        trade.title=                self.link2name(ws[nav['TITLE']+str(index)].value)
        trade.init_price=           ws[nav['INITIAL_PRICE']+str(index)].value
        trade.type =                ws[nav['TYPE']+str(index)].value
        if trade.winner.price == 0:
            #print ("checking if", trade.regnum, " is a winner unknown, inn is ", trade.winner.inn, "price is ", trade.winner.price )
            trade.comment = WINNER_UNKNOWN
            trade.fill = warning_fill

            
        #print ('read ' + trade.regnum + ' with ' + str(len(competitors)) + ' competitors')
        
        return trade
    
    def link2name(self,string):
        # example of link: =HYPERLINK("http://www.spark-marketing.ru/Card/Person/12345","ООО ""ПУПКИН ЛИМИТЕД""")
        res = string.split(',',1)[1]
        res = res.rstrip(')')
        res = res[1:-1]
        res = res.replace('""','"')
        res = res.lstrip ('"')
        #print ('unlinked name: ' + res)
        return res
    
    def get_suspects(self):
        sheetname = self.wb.sheetnames[0]
        print('opening sheet: ' + sheetname)
        ws=self.wb[sheetname]
        string = ws[self.navigation['PROVIDER_INFO']].value
        entries = string.split('; ')
        print (entries)
        inns = [x.split(', ')[1].split(' ')[1] for x in entries]
        print (inns)
        

class Competitor:
    def __init__(self):
        self.title=''
        self.inn=''
        self.price=0
        self.price_drop = 0

class Trade:
    def __init__(self):
        self.title=''
        self.regnum=''
        self.start_date=''
        self.init_price=0
        self.competitors=[]
        self.winner = Competitor()
        self.type = ''
        self.fill = normal_fill
        self.comment = ''
    
    def print_to_ws(self,ws):
        other_competitors = [x for x in self.competitors if x!=self.winner]
        out_index=len(ws['A:A'])+1
        #print (self.regnum+': writing at index '+str(out_index))
        
        ws[OUT_NUMBER_COL+str(out_index)]=              self.regnum
        ws[OUT_TITLE_COL+str(out_index)]=               self.title
        ws[OUT_DATE_COL+str(out_index)]=                str(self.start_date)
        ws[OUT_INITIAL_PRICE_COL+str(out_index)]=       self.init_price
        ws[OUT_WINNER_TITLE_COL+str(out_index)]=        self.winner.title
        ws[OUT_WINNER_PRICE_COL+str(out_index)]=        self.winner.price
        ws[OUT_COMPETITOR_TITLE_COL+str(out_index)]=    "\n".join(map(lambda x: x.title,other_competitors))
        ws[OUT_COMPETITOR_PRICE_COL+str(out_index)]=    "\n".join(map(lambda x: str(x.price),other_competitors))
        ws[OUT_PRICE_DROP_COL+str(out_index)]=          self.winner.price_drop
        ws[OUT_COMMENT_COL+str(out_index)]=             self.comment
        
        for cell in ws[str(out_index)+':'+str(out_index)]: #applying fill to row itself produces ill results :((
            cell.fill = self.fill
            
class Company:
    def __init__(self, inn = ''):
        self.title = ''
        self.inn = inn
        self.ceo = ''
        self.founders = []
        self.phone = ''
        self.source = ''
        self.fill = normal_fill
        
    def print_to_ws(self,ws):
        out_index=len(ws['A:A'])+1
        
        ws[COMPLIST_TITLE_COL+str(out_index)]=               self.title
        ws[COMPLIST_INN_COL+str(out_index)]=                 self.inn
        ws[COMPLIST_DIRECTOR_COL+str(out_index)]=            self.ceo
        ws[COMPLIST_FOUNDER_COL+str(out_index)]=             "\n".join([x.name for x in self.founders])
        ws[COMPLIST_SHARE_COL+str(out_index)]=               "\n".join(['%.2f' % x.share for x in self.founders])
        ws[COMPLIST_PHONE_COL+str(out_index)]=               self.phone
        ws[COMPLIST_SOURCE_COL+str(out_index)]=              '=HYPERLINK("'+self.source+'","'+self.source+'")'
        
        for cell in ws[str(out_index)+':'+str(out_index)]: #applying fill to row itself produces ill results :((
            cell.fill = self.fill

class Founder:
    def __init__(self):
        self.name = ''
        self.share = 0
        
# -----------------------Utils--------------------------------------------

def dt2date(dt):
    return date.fromordinal(dt.toordinal())
    
def shorten_company_title(name):
    """ на вход - длинное имя компании. На выходе - сокращенное
    """
    # добaвлять новые пары key:value здесь
    dictionary_for_short = {
        'ОБЩЕСТВО С ОГРАНИЧЕННОЙ ОТВЕТСТВЕННОСТЬЮ':'ООО',
        'ИНДИВИДУАЛЬНЫЙ ПРЕДПРИНИМАТЕЛЬ':'ИП'
    }
    name_short = name.upper()
    for key in dictionary_for_short.keys():
        if key in name_short:
            name_short = name_short.replace(key, dictionary_for_short[key])

    return name_short

def validate_ogrn_key(key):
    #seems like they are 30 long
    return (len(key)>29)

def validate_number(number):
    # print (len(number))
    if number.isnumeric() and len(number)==19:
        return 1
    # print(str(number)+' is not a valid regnumber')
    return 0

def validate_inn(inn):
    if inn.isnumeric() and len(inn) in [10, 12]: # ten for juridical and 12 for physical
        return 1
    return 0

def print_help():
    print()
    print("Usage: %s [OPTION] SOURCE DEST <suspect-inns ...>" % (cmd))
    print('load anti-cartel form SOURCE, filter it according to suspects and save it as DEST')
    print()
    print('Options:')
    print('\t -h,-H \t --help \t print this help message and do nothing')
    print('\t -c,-C \t --clean \t leave only clean tenders (no manual intervention needed)')
    print('\t -v,-V \t --verbose \t print debug output')
    print('\t  \t --rusprofile \t try fetching company data from rusprofile.ru')
    print('\t  \t --ogrn-online KEY \t try fetching company data from ogrn-online')
    print('\t  \t --check-ogrn-balance \t check number of ogrn requests left (needs enabling ogrn with key)')
    print('\t  \t --template TEMPLATE.docx \t also generate docx legal complaint based on TEMPLATE')

def print_debug(string):
    global verbose
    if verbose:
        print(string)

# ------------------Beautification---------------------------------

def prettify_out_ws_2(ws):

    ws.column_dimensions[OUT_TITLE_COL].width=TITLE_COL_WIDTH_MAX
    for col in COMPANY_TITLE_COLS:
        autofit_column(ws,col,COMPANY_TITLE_COL_WIDTH_MAX)
    
    format_all_cells(ws)
    
    for cell in ws['1:1']:
        cell.font=title_font
        
    for col in NUMBER_COLS+(OUT_COMMENT_COL,):
        autofit_column(ws,col)

    for row_index in range(ws.min_row,ws.max_row+1):
        autofit_row(ws,row_index)

def prettify_out_ws_1(ws):
    
    format_all_cells(ws)
    
    for n in ['1','2']:
        for cell in ws[n+':'+n]:
            cell.font=title_font
        
    for col_n in range(ws.min_column,ws.max_column+1):
        col=get_column_letter(col_n)
        autofit_column(ws,col,COMPANY_TITLE_COL_WIDTH_MAX)

    for row_index in range(ws.min_row,ws.max_row+1):
        autofit_row(ws,row_index)


def format_all_cells(ws):
    
     for col in ws.iter_cols():
        for cell in col:
            cell.font=font
            cell.alignment=alignment
            #cell.fill=ws.row_dimensions[cell.row].fill
            #cell.fill = warning_fill

def autofit_column(ws,letter,max_width=9999999):
    col=ws[letter+':'+letter]
    charnum = 0
    for cell in col:
        string = str(cell.value)
        tokens = string.split('\n')
        for t in tokens:
            if len(t)>charnum:
                charnum=len(t)
    calculated_width = WIDTH_PER_CHAR*charnum+HMARGIN
    ws.column_dimensions[letter].width = min(calculated_width,max_width)

def autofit_row(ws,index):
    row = ws[str(index)+':'+str(index)]
    rownum=0
    for cell in row:
        string = str(cell.value)
        tokens = string.split('\n')
        rows_needed = 0
        for t in tokens:
            # print(cell.column)
            colwidth = ws.column_dimensions[get_column_letter(cell.column)].width
            token_width = len(str(t))*WIDTH_PER_CHAR
            rows_needed+= (-1)*(((-1)*token_width)//(colwidth-1))
        if rows_needed > rownum:
            rownum = rows_needed
    ws.row_dimensions[index].height = rownum*HEIGHT_PER_ROW + (rownum-1)*INTERROW_HEIGHT
        
# -------------------------Core------------------------------------------

def preprocess_trades(trades):
    for t in trades:
        t.type=             t.type.lower()
        t.init_price=       float(t.init_price)
        for c in t.competitors:
            c.title=        shorten_company_title(c.title)
            c.price=        float(c.price)
            c.price_drop=   float(c.price_drop)
            c.inn =         str(c.inn).lstrip('\'')
        if WINNER_UNKNOWN in t.comment: # it might be better to move it to postprocessing
            #print('going through ' + t.regnum)
            #print(t.competitors)
            if len(t.competitors) !=0: 
                min_price = min([x.price for x in t.competitors])
                t.winner = [x for x in t.competitors if x.price == min_price][0]
    pass

def filter_trades(trades):
    #print(str(len(trades)))
    raw_exceptions = [x for x in trades if x.comment != '']
    print ('exceptions before filtering:')
    print("\n".join([(x.regnum + "\t" + x.comment) for x in raw_exceptions if x.comment != '']))
    
    res = [x for x in trades if len(x.competitors)>1] #filter out auctions without competition
    #print(str(len(res)))
    res = [x for x in res if not any([(y.inn not in suspects) for y in x.competitors])] # we need only fully cartel auctions 
    #print(str(len(res)))
    classic_tenders = [x for x in res if 'конкурс открытый' in x.type] # tenders that are not electronic

    #print(str(len(res)))
    res = [x for x in res if 'электрон' in x.type] #leave only auctions
    #print(str(len(res)))
    #print("\n".join([(x.regnum + "\t winner is unknown") for x in res if x.winner.price == 0]))
    
    print ('exceptions after filtering:')
    global exceptional_trades 
    exceptional_trades = [x for x in res if x.comment != '']
    print("\n".join([(x.regnum + "\t" + x.comment) for x in exceptional_trades if x.comment != '']))
    
    if len(classic_tenders):
        print ('cartel-only classic tenders')
        print ('(unfit for a quick complaint, omitted, but interesting):')
        print("\n".join([(x.regnum + "\t" + str(x.init_price) + " RUB") for x in classic_tenders]))

    innocent_suspects = [x for x in suspects if not any([ x in [c.inn for c in t.competitors] for t in res])]
    if len(innocent_suspects):
        print("")
        print("suspects who haven't participated in cartel-only trades")
        print("(one might want to exclude them from the report):")
        #print("\n".join([(x.title + "\t" + x.inn) for x in innocent_suspects]))
        print("\n".join([x for x in innocent_suspects]))

    print("")
    res = [x for x in res if x.comment == '']
    print(str(len(res)) + ' rows after filtering and ' + str(len(exceptional_trades))+ ' exceptions')
    exception_sum = sum([x.init_price for x in exceptional_trades])
    clean_sum = sum([x.init_price for x in res])
    total_sum = exception_sum + clean_sum
    print ('%.2f RUB \t total:' % total_sum)
    print ('%.2f RUB \t in clean tenders' % clean_sum)
    print ('%.2f RUB \t in complicated ones' % exception_sum)
    if not total_sum == 0:
        print ('complicated money ratio: \t %.2f %%' % (exception_sum/total_sum*100))
    return res

def transform_table (in_path, out_path, suspect_list):
    
    print('openining %s' % in_path)
    wb = load_workbook(in_path)
    
    out_wb = Workbook()
    prepare_wb(out_wb, suspect_list)
    out_ws = out_wb['лист 2 (торги)']
    out_complist_ws=out_wb[COMPANY_WS_TITLE]

    source = SourceTable(wb)
    trades = source.to_trades()
    preprocess_trades(trades)
    companies = [Company(x) for x in suspect_list]
    for c in companies: 
        find_inn_title(c,trades)
        if rusprofile_requests:
            rusprofile_extract_info(c)
            sleep(COOLDOWN)
        if ogrn_requests:
            ogrn_online_extract_info(c)
        c.print_to_ws(out_complist_ws)
    
    trades=filter_trades(trades)
    
    if complicated_enabled:
        for t in exceptional_trades:
            t.print_to_ws(out_ws)
    for t in trades:
        t.print_to_ws(out_ws)
    
    write_end_sum(out_ws,2,OUT_INITIAL_PRICE_COL)
    prettify_out_ws_1(out_complist_ws)
    prettify_out_ws_2(out_ws)
    out_wb.save(out_path)
    
    if template_file != '':
        doc = generate_docx (template_file, trades, companies)
        doc.save(docx_out)

def write_end_sum(ws,start_index,col_letter):
    index=len(ws[col_letter+':'+col_letter])+1
    ws[col_letter+str(index)] = '=SUM('+col_letter+str(start_index)+':'+col_letter+str(index-1)+')'


def prepare_wb (workbook, suspect_list):
    ws = workbook.active
    ws.title = COMPANY_WS_TITLE
    ws2 = workbook.create_sheet(AUCTIONS_WS_TITLE)
    ws2[OUT_NUMBER_COL+'1'] =           'Номер извещения'
    ws2[OUT_TITLE_COL+'1'] =            'Наименование закупки'
    ws2[OUT_DATE_COL+'1'] =             'Дата публикации'
    ws2[OUT_INITIAL_PRICE_COL+'1'] =    'НМЦ (руб.)'
    ws2[OUT_WINNER_TITLE_COL+'1'] =     'Победитель'
    ws2[OUT_WINNER_PRICE_COL+'1'] =     'Цена победителя'
    ws2[OUT_COMPETITOR_TITLE_COL+'1'] = 'Другие участники'
    ws2[OUT_COMPETITOR_PRICE_COL+'1'] = 'Цена'
    ws2[OUT_PRICE_DROP_COL+'1'] =       'Снижение, %'
    
    ws['A1'] = 'Приложение, Таблица 1 на двух листах '
    ws[COMPLIST_TITLE_COL+'2'] =        'Компания'
    ws[COMPLIST_INN_COL+'2'] =          'ИНН'
    ws[COMPLIST_DIRECTOR_COL+'2'] =     'Генеральный директор'
    ws[COMPLIST_FOUNDER_COL+'2'] =      'Учредитель'
    ws[COMPLIST_SHARE_COL+'2'] =        'Доля, %'
    ws[COMPLIST_PHONE_COL+'2'] =        'Дополнительная информация'
    ws[COMPLIST_SOURCE_COL+'2'] =       'Источник'

def find_inn_title(company,trades):
    for t in trades:
        for c in t.competitors:
            if c.inn == company.inn:
                company.title = c.title
             
def crosslist_compare(clean_list,dirty_list,func_of_iter_or_2,stringifier):
    
    # with min and max in mind but any other function of same format will do
    
    clean_value = func_of_iter_or_2(clean_list)
    dirty_value = clean_value
    
    if len(dirty_list)!=0:
        dirty_value = func_of_iter_or_2(dirty_list)
        
    final_value = func_of_iter_or_2(clean_value,dirty_value)
    string_value = stringifier(final_value)
    
    if len(dirty_list)!=0 and final_value == dirty_value: #if dirtylist is empty dirty is always same as clean
        print ("WARNING: dirty value in text")
        string_value = string_value + "/DIRTY/"
    
    return string_value

def instancelist_compare(subfield_selector,clean_list,dirty_list,func_of_iter_or_2,stringifier):
    list_list = [[subfield_selector(x) for x in y] for y in [clean_list,dirty_list]]
    return crosslist_compare(list_list[0],list_list[1] if complicated_enabled else [],func_of_iter_or_2,stringifier)
    

def generate_docx (template, trades, companies):
    global exceptional_trades
    doc = DocxTemplate(template)
    
    company_names = [x.title+" (ИНН "+x.inn+")" for x in companies]
    company_list = ", ".join(company_names)
    vertical_company_listing = Listing("\a\a\t".join(company_names))
    
    min_pricedrop = instancelist_compare((lambda x: x.winner.price_drop),trades,exceptional_trades, min, (lambda x : "%.2f" % x))
    max_pricedrop = instancelist_compare((lambda x: x.winner.price_drop),trades,exceptional_trades, max, (lambda x : "%.2f" % x))
    
    pricedrop_range = min_pricedrop+'-'+max_pricedrop+'%'
    
    min_date = instancelist_compare((lambda x: x.start_date),trades,exceptional_trades, min, str)
    max_date = instancelist_compare((lambda x: x.start_date),trades,exceptional_trades, max, str)
    
    
    time_period = "В период c " + min_date + ' по ' + max_date #TODO: if truncating to a year it might be a SINGLE year. 
    
    context = {
            'time_period' : time_period,
            'vertical_company_listing' : vertical_company_listing,
            'company_list' : company_list,
            'pricedrop_range' : pricedrop_range 
            }
    doc.render(context)
    
    return doc

# ------------------ Network ---------------------------------

def ogrn_online_extract_info(company):
    
    #step 1: retrieving id (like https://xn--c1aubj.xn--80asehdb/интеграция/#company_search_link) 
    #There is also a relative 'url' field (starts with /)
    
    minimal_info = request_ogrn_online('компании/?инн='+company.inn)
    #print (minimal_info)
    
    if len(minimal_info) == 0:
        print ('cannot find ogrn id for ' + company.inn)
        return
    
    ogrn_id = str(minimal_info[0]['id'])
    company_shortname = minimal_info[0]['shortName']
    
    print ('found id = ', ogrn_id)
    company.source = generate_ogrn_link(ogrn_id,company_shortname)
    
    #step 2: retrieving general info (like https://xn--c1aubj.xn--80asehdb/интеграция/#company_link)
    
    general_info = request_ogrn_online('компании/'+ogrn_id+'/')
    print (general_info)
    if len(general_info) == 0:
        print ('cannot retreive general info for ' + company.inn)
        return
    
    if 'акционер' in general_info['okopf']['name'].lower():
         company.fill = error_fill
    
    capital = float(general_info['authorizedCapital']['value'])
    
    #step 3: retrieving ceo (like https://xn--c1aubj.xn--80asehdb/интеграция/#company_employees_link)
    
    personnel_info = request_ogrn_online('компании/'+ogrn_id+'/сотрудники/')
    if len(personnel_info) == 0:
        print ('cannot retreive personnel info for ' + company.inn)
        return
    ceos=[x for x in personnel_info if x['post']['code'] in ['02']]
    
    if len(ceos) == 0:
        print ('cannot find ceo for ' + company.inn)
        return
    ceo=ceos[0]['person']
    company.ceo = name2str(ceo)

    
    #step 4: retrieving founders. In today's API there is an undocumented 'pricePercent' field. But not for companies!
    
    founders_info = request_ogrn_online('компании/'+ogrn_id+'/учредители/')
    for f in founders_info:
        #print(f)
        founder = Founder()
        if 'personOwner' in f.keys():
            founder.name = name2str(f['personOwner'])
        if 'companyOwner' in f.keys():
            founder.name = f['companyOwner']['shortName']
        if 'price' in f.keys():
            founder.share = float(f['price'])/capital*100
        company.founders.append(founder)

def name2str(dic):
    return " ".join([dic['surName'].upper(),dic['firstName'].upper(),dic['middleName'].upper()])

def generate_ogrn_link(ogrn_id,name):
    nameslug = name.strip().replace('"','').replace(' ','_').lower()
    link = OGRN_ROOT+'компании/'+ogrn_id+'-'+nameslug+'/'
    return link

def request_ogrn_online(query):
    
    try:
        print ('Quering OGRN with: '+query)
        headers = {
                'User-Agent': USER_AGENT,
                'X-ACCESS-KEY': ogrn_key}
        res = requests.get(OGRN_COMMON+query, headers=headers, timeout=60)
        res.raise_for_status()
        obj = json.loads(res.text)
        sleep(0.4)
        return obj
    
    except Exception as e:
        print(query+' cannot be completed')
        print(e)

def check_ogrn_balance():
    account_info = request_ogrn_online('профиль/счёт/')
    if len(account_info) == 0:
        print ('cannot check balance')
        return
    print ('you have approximately '+ str(account_info['requestsLeft']) + ' requests left')


def rusprofile_extract_info(company):
    
    inn = company.inn
    if len(inn) == 12:
        print('not looking up individual ' + inn)
        return
    query_result = load_url(YAHOOCOJP_COMMON+RUSPROFILE_SHORT+'+'+str(inn))
    #print(query_result)
    start = query_result.find(RUSPROFILE_COMMON.replace('https://',''))
    #print ('startposition = ', str(start))
    end= query_result.find('"',start)
    #print ('endposition = ', str(end))
    link = query_result[start:end]
    if link == '':
        print("can't find rusprofile link for " + str(inn) + ' with yahoo.co.jp')
    else:
        print("rusprofile link found for " + str(inn))
    link = 'https://' + link
    company.source = link
    parse_rusprofile(link,company)
     
def load_url(url):
    try:
        print ('loading url: '+url)
        headers = {'User-Agent': USER_AGENT}
        res = requests.get(url, headers=headers, timeout=60)
        res.raise_for_status()
        return res.text

    except Exception as e:
        print(url+'not responding')
        print(e)

def parse_rusprofile(url,company,filename='',check_inn = True):
    text = ''
    if filename != '': # for testing purposes
        file=open(filename)
        text = file.read()
        file.close()
    else:
        text = load_url(url)
    soup = BeautifulSoup(text,'html.parser')
    company.name=list(soup.find(attrs={"itemprop": "legalName"}).stripped_strings)[0]
    company.ceo = list(soup.find(attrs={"class":"multiceo expandable-element"}).find(attrs={"itemprop": "name"}).stripped_strings)[0]   
    #ООО  &quot;EXAMPLE&quot;, Город (ИНН 1234567890, ОГРН 1234567890123)
    page_inn = soup.title.string.split('ИНН ')[1].split(',')[0]
    print('on-page inn = ' + page_inn)
    if page_inn != company.inn and check_inn:
        print("WARNING: rusprofile page scrambling is in effect, can't extract company info.")
        print("This might also mean that your IP will need to use Tor browser to use rusprofile for a day or so")
        return
    #print(str(nametag.string))
    kinda_founders_probably = soup.find_all(attrs={"data-loader-section": "founders"})
    print ('kinda_founders length = ', len(kinda_founders_probably))
    protofounders = []
    for f in kinda_founders_probably:
        print (f.prettify())
        names = [list(x.stripped_strings)[0] for x in f.find_all(itemprop="name")]
        print ('names', names)
        moneys = f.find_all (attrs={"class": "i-money"})
        #print ('moneys', moneys)
        shares = [list(x.find_all(attrs={"class": "i-text"})[1].stripped_strings)[0] for x in moneys]
        protofounders.extend(zip(names,shares))
    print ('protofounders length = ', len(protofounders))
    founders = []
    for p in protofounders:
        founder = Founder()
        founder.name = p[0]
        founder.share = p[1].rstrip(' %')
        founders.append(founder)
    company.founders = founders
    print(company.ceo,[x.name for x in company.founders],[x.share for x in company.founders])

def parsetest():
    company = Company()
    parse_rusprofile('does not matter',company,'testhtml', False)
    parse_rusprofile('does not matter',company,'testhtml2', False)
    parse_rusprofile('does not matter',company,'testhtml3', False)
    #print(company.ceo,[x.name for x in company.founders],[x.share for x in company.founders])

#    for f in kinda_founders_probably:
#        #print(f.prettify())
#        for a in f.find_all('a'):
#            print(str(a.string))

# ------------------ MAIN---------------------------------

index = 1

if len(sys.argv) == 1:
    print('Error: need two arguments + suspects')
    print_help()
    sys.exit(1)

if len(sys.argv) == 2 :
    if sys.argv[1] in ['-h', '-H','--help']:
        print_help()
        sys.exit(0)
    else:
        print('Error: need two arguments + suspects')
        print_help()
        sys.exit(1)

while index < len(sys.argv):
    arg = sys.argv[index]
    index += 1
    if arg in ['-h', '-H','--help']:
        print_help()
        sys.exit(0)
    
    elif arg in ['-c', '-C','--clean']:
        complicated_enabled = False
    
    elif arg in ['-v', '-V','--verbose']:
        verbose = True
        
    elif arg in ['--ogrn-online']:
        ogrn_requests = True
        ogrn_key = sys.argv[index]
        if not validate_ogrn_key(ogrn_key):
            print(ogrn_key + ' is not a valid key')
            sys.exit(1)
        index += 1
        continue
    
    elif arg in ['--template']:
        template_file = sys.argv[index]
        if not '.docx' in template_file:
            print(ogrn_key + ' is not a valid docx template')
            sys.exit(1)
        index += 1
        continue
    
    elif arg in ['--check-ogrn-balance']:
        if not ogrn_requests:
            print('you need to specify the key')
            sys.exit(1)
        check_ogrn_balance()
        sys.exit(0)
        
    elif target_file == '':
        target_file = arg
    elif output_file == '':
        output_file = arg
    elif validate_inn(arg):
        suspects.append(arg)
    else:
        print("%s is not a valid inn" % arg)
        sys.exit(1)

if ogrn_requests and rusprofile_requests:
    print("please choose only ONE source of company data retrieval")
    sys.exit(1)

if len(suspects) < 2:
    print ("At least 2 suspects needed! \n Not much of a cartel otherwise!")
    sys.exit(1)
# maybe someday output_file = target_file
if template_file != '':
    if ".xlsx" in output_file:
        docx_out = output_file.replace(".xlsx",".docx")
    else:
        docx_out = output_file + ".docx"

transform_table(target_file, output_file, suspects)
#parsetest()
print ('successfully completed')
sys.exit(0)
