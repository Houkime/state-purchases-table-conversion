## Requirements (for .py, .exe does not need anything):

* Python 3.x
* openpyxl library:
`$ pip install openpyxl`
* requests (for searching for rusprofile and ogrn-online.)
`$ pip install requests`
* bs4 (for going through fetched html. TODO: POTENTIALLY NO LONGER USED)
`$ pip install bs4`
* simplejson (for using web API of ogrn-online)
`$ pip install simplejson`
* docxtpl (for using a configurable docx template with jinja)
`$ pip install docxtpl`

In one non-sudo command:

```
$ pip install --user openpyxl requests bs4 simplejson docxtpl
```

## Usage:

This is a CLI application meant to be called from terminal or another program.
You can receive up-to-date reference by using builtin help:

### Unix-like:

`$ ./filter.py --help`

### Windows (for compiled exe, check the releases section for download):

`.\filter.exe --help`

```
Usage: ./filter.py [OPTION] SOURCE DEST <suspect-inns ...>
load anti-cartel form SOURCE, filter it according to suspects and save it as DEST

Options:
	 -h,-H 	 --help 	 print this help message and do nothing
	 -c,-C 	 --clean 	 leave only clean tenders (no manual intervention needed)
	  	 --rusprofile 	 try fetching company data from rusprofile.ru
	  	 --ogrn-online KEY 	 try fetching company data from ogrn-online
	  	 --check-ogrn-balance 	 check number of ogrn requests left (needs enabling ogrn with key)
	  	 --template TEMPLATE.docx 	 also generate docx legal complaint based on TEMPLATE
```

### (may be outdated) example:

```
./filter.py input.xlsx output.xlsx 1234567891 2345678912 3456789123 456791234
```

where numbers are inns of suspects.

### Filtering:

To be eligible for legal action, trades should satisfy some requirements.
Currently, the following filters are applied to the returned results:

* Trades should not incude any competitor from outside of a supposed cartel
* Trades should be in a form of electronic auction so that Federal Law No. 135 1.11 applies.
* Trades should include at least 2 competitors.
* Banned competitors do not count.

### Exceptional trades:

These are most commonly artifacts of the database or special cases.

Those exceptions that survive the above filtering can be dealt with manually (by trying to repair the input file 
or by other means) or be just omitted if `--clean` flag is used.

There is no penalty for not including some misbehavior of the suspects,
but sometimes other evidence is not enough so one needs to investigate rare complicated cases.

Exceptional trades can sometimes indicate more subtle violations by suspects.

### Notes:

* Input SHOULD be in .xlsx format (limitation of openpyxl)
* Input can currently be either from Spark or Ist-budget. It is detected automatically.
