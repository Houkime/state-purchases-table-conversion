#!/usr/bin/bash

# an example of quick build script for x64 and x32 win under *nix

WINEPREFIX=~/CompileWine wine ~/CompileWine/drive_c/Python37/Scripts/pyinstaller.exe --onefile filter.py
WINEPREFIX=~/CompileWine32 wine ~/CompileWine32/drive_c/Python37/Scripts/pyinstaller.exe --onefile --distpath ./dist32 --workpath ./build32 filter.py